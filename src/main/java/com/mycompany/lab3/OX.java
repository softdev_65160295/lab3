/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author natta
 */
class OX {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }
        if (checkCol(table, currentPlayer)) {
            return true;
        }
        if (checkX1(table, currentPlayer)) {
            return true;
        }
        if (checkX2(table, currentPlayer)) {
            return true;
        }
        if (checkDraw(table)){
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        for (int i = 0; i < 3; i++) {
            if (!table[row][i].equals(currentPlayer)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        for (int i = 0; i < 3; i++) {
            if (!table[i][col].equals(currentPlayer)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1(String[][] table, String currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (!table[i][i].equals(currentPlayer)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(String[][] table, String currentPlayer) {
        if(table[0][2].equals(currentPlayer)&&table[1][1].equals(currentPlayer)&&table[2][0].equals(currentPlayer)){
            return true;
        }
        return false;
    }

    static boolean checkDraw(String[][] table) {
        int turn =0;
        for(int i =0;i<3;i++){
            for(int j = 0;j<3;j++){
                if(table[i][j]!="-"){
                    turn++;
                }
            }
        }
        if(turn==9){
            return true;
        }
        return false;
    }
    
}
